<?php
require_once("class.php");
$value= new Person();
$value->connect();
$id=$_GET['id'];
$result= $value-> view_selective($id);
?>
<!DOCTYPE html>
<html>
<head>
  <title>Insert </title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
	<div class="container">
  <form method="POST" action="insert_process.php">
  <div class="form-group">
    <input name="id" type="hidden" class="form-control">
    <label for="exampleFormControlInput1">Name</label>
    <input name="name" type="text" class="form-control" id="exampleFormControlInput1" value ="<?php echo $result['name']; ?>">
    <label for="exampleFormControlInput1">Age</label>
    <input name="age" type="text" class="form-control" id="exampleFormControlInput1" value ="<?php echo $result['age']; ?>">
    <label for="exampleFormControlInput1">Address</label>
    <input name="address" type="text" class="form-control" id="exampleFormControlInput1" value ="<?php echo $result['address']; ?>">
    <button type="Submit" class="btn btn-primary btn-lg btn-block">Submit</button>
  </div>
</form>
</div>

</body>
</html>